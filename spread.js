if (Drupal.jsEnabled) {
  $(document).ready(function(){
    $("#spread-form").submit(function(){
      $("#spread-thank-you").hide();
      $("#spread-form input.form-text").each(function(){
        $(this).removeClass("error");
      });
      data = "";
      $("#spread-form input.form-text").each(function(){
        data += "&" + $(this).attr('name') + "=" + $(this).attr('value');
      });
      data += "&url=" + Drupal.q;
      $.ajax({
        type: "POST",
        url: Drupal.settings.basePath + "?q=spread/js",
        data: data,
        success: function(msg){
          if (msg == 'TRUE') {
            $("#spread-thank-you").show();
          }else{
            $("#spread-form input.form-text").each(function(){
              $(this).addClass("error");
            });
          };
        }
      });
      return false;
    });
  });
};