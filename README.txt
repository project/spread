
Drupal Spread module:
----------------------
Original author - Jérémy Chatard http://www.breek.fr
Author - Jérémy Chatard jchatard at breek dot fr
Requires - Drupal 6
License - GPL (see LICENSE)


Overview:
--------
This module provides a block allowing users to spread the word
about your website. Think of it as Forward module but as simple
as it sould be!

If the user has a JavaScript enabled browser, the block will submit
through Ajax. If no JavaScript is present, then it will degrade gracefully.

Installation:
------------
1. Copy the spread directory to the Drupal sites/all/modules/ or sites/example.com/modules/ directory.
2. Go to "administer" -> "modules" and enable the module.
3. Check the "use spread form" -> "access control" page to enable use of
   this module to different roles.
4. Set up your email messages and fields count in "administer" -> "site configuration" -> "Spread"
5. Enable the "Spread form" under "site building" -> "blocks"
